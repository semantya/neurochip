#!/bin/bash

scrapyctl () {
    ENDPOINT=crawlers.scubadev.com
    #ENDPOINT=scuba-octopus.herokuapp.com

    PROJECT=octopus

    scrapy $*
}

dj_manage () {
    python manage.py $*
}

################################################################################

export CORTEX_ROOT=$PWD

export CORTEX_BOOT=$CORTEX_ROOT/boot
export CORTEX_VENV=$CORTEX_ROOT/venv

#*******************************************************************************

if [[ $UCHIKOMA_NODE != "" ]] ; then
    export UCHIKOMA_HOME=home/$UCHIKOMA_NODE

    if [[ ! -d $UCHIKOMA_HOME ]] ; then
        git clone https://bitbucket.org/tayaa/$UCHIKOMA_NODE.git $UCHIKOMA_HOME --recursive --depth 1
    fi
else
    export UCHIKOMA_NODE=satellite
    export UCHIKOMA_HOME=opt/satellite
fi

export UCHIKOMA_NARROW=$UCHIKOMA_CHIP"://"$UCHIKOMA_NODE"@"$UCHIKOMA_ROOT"/"$UCHIKOMA_VERSE

#*******************************************************************************

export CORTEX_ENV=debug

if [[ -f $CORTEX_BOOT/env/$CORTEX_ENV.sh ]] ; then
    source $CORTEX_BOOT/env/$CORTEX_ENV.sh
fi

#*******************************************************************************

for key in agora anatomy wisdom ; do
    source boot/core/$key.sh
done

