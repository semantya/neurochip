#!/bin/bash

export ALL_LOBEs="story converse reasoner witness seeker spine face head"

################################################################################

uchikoma_cortex_story () {
    export PORT=$BASE_PORT"1"
}

#*******************************************************************************

uchikoma_cortex_converse () {
    export PORT=$BASE_PORT"2"
}

#*******************************************************************************

uchikoma_cortex_reasoner () {
    export PORT=$BASE_PORT"3"
}

#*******************************************************************************

uchikoma_cortex_witness () {
    export PORT=$BASE_PORT"4"
}

#*******************************************************************************

uchikoma_cortex_seeker () {
    export PORT=$BASE_PORT"5"
}

#*******************************************************************************

uchikoma_cortex_spine () {
    export PORT=$BASE_PORT"6"
}

#*******************************************************************************

uchikoma_cortex_face () {
    export PORT=$BASE_PORT"7"
}

#*******************************************************************************

uchikoma_cortex_head () {
    export PORT=$BASE_PORT"8"
}

