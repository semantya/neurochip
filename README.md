# About

Gestalt is a dynamic application, allowing easy deployement of AI-like spots. Each spot has a frontoffice and backoffice, which can be configured through $CORTEX_FRONT and $CORTEX_BACK.

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy?template=https://github.com/neurotics/gestalt.git)

# Modules :

## Core :

*) satellite  :
Web server.

*) knowledge  :
Web server.

*) gestalt :
Web server.

## Extensions :

*) spine  :
Web server.

*) face  :
Web server.

*) head  :
Web server.

## Cortex :

*) Converse  :
Web server.

*) Reasoner  :
Web server.

*) Witness  :
Web server.

*) Seeker  :
Web server.

## Frameworks :

*) Slack :
Real-time bot.

*) airflow :
Web server.

*) luigi :
Web server.

*) scrapy :
Web server.

